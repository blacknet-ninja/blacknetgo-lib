GOCMD=GO111MODULE=on go
GOBUILD=$(GOCMD) build
GOTEST=$(GOCMD) test
GOOS=$(shell go env | grep GOOS | awk -F "=" '{print $$NF}' | awk -F "\"" '{print $$2}')
GOARCH=$(shell go env | grep GOARCH | awk -F "=" '{print $$NF}' | awk -F "\"" '{print $$2}')
test:
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOTEST) -v ./...
cover:
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOTEST) -cover ./...
benchmark:
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOTEST) -bench=. -benchmem ./...
git:
	git add .
	git commit -m "update"
	git push origin master
