package bip39

import (
	"encoding/binary"
	"math/rand"
	"strings"

	"golang.org/x/crypto/blake2b"
)

func genRandomBytes(size int) []byte {
	blk := make([]byte, size)
	rand.Read(blk)
	return blk
}

func checkVersion(sk [32]byte) bool {
	return (sk[0] & 0xF0) == 0x10
}

// NewMnemonic new a blacknet mnemonic
func NewMnemonic() string {
	mnemonic := ""
	for {
		seed := genRandomBytes(12 * 2)
		var words []string
		for i := range seed {
			if i%2 == 0 {
				words = append(words, English[binary.BigEndian.Uint16(seed[i:i+2])%uint16(len(English))])
			}
		}
		mnemonic = strings.Join(words, " ")
		hash := blake2b.Sum256([]byte(mnemonic))
		if checkVersion(hash) {
			break
		}
	}
	return mnemonic
}
