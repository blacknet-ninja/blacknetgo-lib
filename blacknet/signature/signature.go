package signature

// Signature .
type Signature [64]byte

// Empty .
func Empty() Signature {
	return Signature{}
}
