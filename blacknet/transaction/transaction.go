package transaction

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/hash"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/signature"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/utils"
)

// Transaction .
type Transaction struct {
	Signature signature.Signature
	From      string
	Seq       uint32
	Hash      hash.Hash
	Fee       uint64
	Type      uint8
	Data      []byte
}

// New new a transfer
func New(from string, seq uint32, hash hash.Hash, fee uint64, t uint8, data []byte) *Transaction {
	return &Transaction{
		Signature: signature.Empty(),
		From:      utils.PublicKeyToHex(utils.PublicKey(from)),
		Seq:       seq,
		Hash:      hash,
		Fee:       fee,
		Type:      t,
		Data:      data,
	}
}

// Serialize .
func (s *Transaction) Serialize() []byte {
	buf := &bytes.Buffer{}
	binary.Write(buf, binary.BigEndian, s.Signature)
	binary.Write(buf, binary.BigEndian, utils.HexToPublicKey(s.From))
	binary.Write(buf, binary.BigEndian, s.Seq)
	binary.Write(buf, binary.BigEndian, s.Hash)
	binary.Write(buf, binary.BigEndian, s.Fee)
	binary.Write(buf, binary.BigEndian, s.Type)
	// data
	binary.Write(buf, binary.BigEndian, utils.EncodeVarInt(len(s.Data)))
	binary.Write(buf, binary.BigEndian, s.Data)
	return buf.Bytes()
}

// Deserialize .
func Deserialize(arr []byte) *Transaction {
	s := signature.Signature{}
	h := hash.Hash{}
	copy(s[:], arr[0:64])
	copy(h[:], arr[100:132])
	var t uint8
	buf := bytes.NewReader(arr[140:141])
	binary.Read(buf, binary.BigEndian, &t)
	return &Transaction{
		Signature: s,
		From:      utils.PublicKeyToHex(arr[64:96]),
		Seq:       binary.BigEndian.Uint32(arr[96:100]),
		Hash:      h,
		Fee:       binary.BigEndian.Uint64(arr[132:140]),
		Type:      t,
		Data:      arr[len(arr)-utils.DecodeVarInt(arr[141:]):],
	}
}
