package blacknet

import (
	"encoding/hex"
	"strings"
	"testing"
)

const (
	testAccount    = "blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036"
	testMnemonic   = "piano maze provide discover tower scissors true leave senior aware secret film"
	testPkHex      = "abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df3404"
	testSkHex      = "1e662e8fc3df898cc86777af5b3711c5115ed49c302d47fcb164cea5f18ea2c7abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df3404"
	testMessage    = "BLN-is-very-nice"
	testSign       = "FF6D74C0493720F59DA4F06CD6D13D4A47D04F61E6D4AFF3D001EBD59153DC6DF40DDA34F7CD63FBA76CD2C1CA3963D1CB4F17F2061FB191BA441F0BF925C40B"
	testSerialized = "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080"
	testSignature  = "7cad618727b1dd3872d685c4c2eba86843e64321fe622a60a66807e5a495a7b081a04eb8eebd84b683d2cbad8b13ce6277b4e2280ec6da528d0b9f344f246a0babb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080"
	testEncrypted  = "669E238CBD3977550DEAB18DEE256BCB16748CA9"
	testStr        = "blacknet"
	testMnemonic2  = "prepare long erode easy moment dinosaur soft sound exhibit wire mesh muffin"
	testAccount2   = "blacknet1pns3tp3wja8jmqqyfy0pvqedg9nv4zj3tkcpqmep5hr3rvw2rnkqhafpf9"
)

func Test_Blacknet_EncryptDecrypt(t *testing.T) {
	encrypt := Encrypt(testMnemonic2, testAccount, testStr)
	decrypt := Decrypt(testMnemonic, testAccount2, encrypt)
	if decrypt != testStr {
		t.Errorf("Encrypt(%s,%s,%s) = %v; Decrypt(%s,%s,%s) expected %v", testMnemonic2, testAccount, testStr, encrypt, testMnemonic, testAccount2, encrypt, decrypt)
	}
}

func Test_Blacknet_Encrypt(t *testing.T) {
	encrypt := Encrypt(testMnemonic2, testAccount, testStr)
	if len(encrypt) != 40 {
		t.Errorf("Encrypt(%s,%s,%s) = %v", testMnemonic2, testAccount, testStr, encrypt)
	}
}

func Benchmark_Blacknet_Encrypt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Encrypt(testMnemonic2, testAccount2, testStr)
	}
}

func Test_Blacknet_Decrypt(t *testing.T) {
	str := Decrypt(testMnemonic2, testAccount2, testEncrypted)
	if str != testStr {
		t.Errorf("Decrypt(%s,%s,%s) = %v; expected %v", testMnemonic2, testAccount2, testEncrypted, str, testStr)
	}
}

func Benchmark_Blacknet_Decrypt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Decrypt(testMnemonic2, testAccount2, testEncrypted)
	}
}

func Test_Blacknet_Signature(t *testing.T) {
	signature := Signature(testMnemonic, testSerialized)
	if signature != testSignature {
		t.Errorf("Signature(%s,%s) = %v; expected %v", testMnemonic, testSerialized, signature, testSignature)
	}
}

func Benchmark_Blacknet_Signature(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Signature(testMnemonic, testSerialized)
	}
}

func Test_Blacknet_Verify(t *testing.T) {
	ok := Verify(testAccount, testSign, testMessage)
	if !ok {
		t.Errorf("Verify(%s,%s,%s) = %v; expected %v", testAccount, testSign, testMessage, ok, true)
	}
}

func Benchmark_Blacknet_Verify(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Verify(testAccount, testSignature, testMessage)
	}
}

func Test_Blacknet_Sign(t *testing.T) {
	sign := Sign(testMnemonic, testMessage)
	expected := testSign
	if sign != expected {
		t.Errorf("Sign(%s,%s) = %s; expected %s", testMnemonic, testMessage, sign, expected)
	}
}

func Benchmark_Blacknet_Sign(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Sign(testMnemonic, testMessage)
	}
}

func Test_Blacknet_Hash(t *testing.T) {
	hash := Hash(testMnemonic)
	expected := "1e662e8fc3df898cc86777af5b3711c5115ed49c302d47fcb164cea5f18ea2c7"
	if hex.EncodeToString(hash) != expected {
		t.Errorf("Hash(%s) = %x; expected %s", testMnemonic, hash, expected)
	}
}

func Benchmark_Blacknet_Hash(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Hash(testMnemonic)
	}
}

func Test_Blacknet_Ed25519(t *testing.T) {
	pk, sk, err := Ed25519(testMnemonic)
	if err != nil {
		t.Errorf("Ed25519(%s) Output Error: %s", testMnemonic, err.Error())
	}
	if hex.EncodeToString(pk) != testPkHex || hex.EncodeToString(sk) != testSkHex {
		t.Errorf("Ed25519(%s) = %x,%x; expected %s,%s", testMnemonic, pk, sk, testPkHex, testSkHex)
	}
}

func Benchmark_Blacknet_Ed25519(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Ed25519(testMnemonic)
	}
}

func Test_Blacknet_Address(t *testing.T) {
	address := Address(testMnemonic)
	if address != testAccount {
		t.Errorf("Address(%s) = %s; expected %s", testMnemonic, address, testAccount)
	}
}

func Benchmark_Blacknet_Address(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Address(testMnemonic)
	}
}

func Test_Blacknet_NewMnemonic(t *testing.T) {
	mnemonic := NewMnemonic()
	words := strings.Split(mnemonic, " ")
	if len(words) != 12 {
		t.Errorf("NewMnemonic() Output: %s", mnemonic)
	}
}

func Benchmark_Blacknet_NewMnemonic(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewMnemonic()
	}
}
